package com.example.BlogRestaurante.services;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}