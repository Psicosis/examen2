package com.example.BlogRestaurante.services;

import com.example.BlogRestaurante.entities.User;
import com.example.BlogRestaurante.repositories.RoleRepository;
import com.example.BlogRestaurante.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service("UserService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);
    }

    @Override
    public Iterable<User> listAllUsers() {
        return null;
    }

    @Override
    public void saveUser(User user) {

    }

    @Override
    public User getUserById(Integer id) {
        return userRepository.findOne(id);
    }
    @Override
    public void deleteUser(Integer id) {
        userRepository.delete(id);
    }
    @Override
    public Iterable<User> listAllRestaurants() {return userRepository.findAll();}

    @Override
    public void saveUserEdited(User user){
        userRepository.save(user);
    }


    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}