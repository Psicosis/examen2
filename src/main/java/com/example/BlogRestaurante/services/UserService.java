package com.example.BlogRestaurante.services;

import com.example.BlogRestaurante.entities.User;

/**
 * Created by amolina on 10/05/17.
 */
public interface UserService {
    void save(User user);
    Iterable<User> listAllUsers();
    void saveUser(User user);

    User getUserById(Integer id);
    void deleteUser(Integer id);

    Iterable<User> listAllRestaurants();

    void saveUserEdited(User user);



    User findByUsername(String username);
}