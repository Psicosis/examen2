package com.example.BlogRestaurante.controllers;



import com.example.BlogRestaurante.entities.User;
import com.example.BlogRestaurante.services.SecurityService;
import com.example.BlogRestaurante.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    //@Autowired
    //private UserValidator userValidator;


    @RequestMapping("/paginaPrincipal")
    public String viewMain() {

        return "view";
    }
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationInit(Model model) {
        model.addAttribute("user", new User());

        return "registration";
    }


    @RequestMapping(value = "/users",  method = RequestMethod.GET)
    public String listUsers(Model model) {
        model.addAttribute("users", userService.listAllUsers());

        return "users";
    }



    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("user") User user, BindingResult bindingResult, Model model) {
        ///userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userService.save(user);
        securityService.autologin(user.getUsername(), user.getPasswordConfirm());
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "welcome";
    }

    @RequestMapping(value = {"/admin/restaurants/"}, method = RequestMethod.GET)
    public String admin(Model model) {
        return "restaurants";
    }


    @RequestMapping(value = {"/bienvenidos"}, method = RequestMethod.GET)
    public String welcome2(Model model) {
        return "bienvenidos";
    }



    @RequestMapping(value = "/admin/users",  method = RequestMethod.GET)
    public String listRestaurants(Model model) {
        model.addAttribute("users", userService.listAllUsers());

        return "restaurants";
    }


}