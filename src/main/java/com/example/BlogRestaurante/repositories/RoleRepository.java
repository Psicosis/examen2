package com.example.BlogRestaurante.repositories;

import com.example.BlogRestaurante.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface RoleRepository extends JpaRepository<Role, Integer> {

}